#!/bin/bash
baseurl=http://vorlesungsplan.dhbw-mannheim.de/ical.php?uid=

function remove_duplicate() {
	duplicate=$(grep '^UID:' $1 | sort | uniq -d)
	[[ -z $duplicate ]] && return
	for line in $duplicate
	do
		echo $line
		old_id=$(echo $line | grep -oP '(?<=-)[0-9]+(?=@)')
		new_line=$(echo $line | sed "s/$old_id/$((old_id + 1))/")
		sed -i "0,/$line/ s/$line/$new_line/" $1
	done
	remove_duplicate $1
}

for id in $(cat ids.txt)
do
	filename=ical/${id}.ical
	wget -O $filename ${baseurl}${id}
	sed -i 's/Securiy /Security /g' $filename
	remove_duplicate $filename &
done
