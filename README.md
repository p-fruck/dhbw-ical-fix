## iCal fix for DHBW

Please select your iCal file by the corresponding link. (pipeline runs daily)

## Courses TINF,TAI,TIT
[TINF18 AI1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7430001.ical?job=update_calendar)

[TINF18 AI2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7431001.ical?job=update_calendar)

[TINF18 IT1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7432001.ical?job=update_calendar)

[TINF18 IT2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7433001.ical?job=update_calendar)

[TINF19 AI1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7757001.ical?job=update_calendar)

[TINF19 AI2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7758001.ical?job=update_calendar)

[TINF19 CS1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7759001.ical?job=update_calendar)

[TINF19 IT1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7760001.ical?job=update_calendar)

[TINF19 IT2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7761001.ical?job=update_calendar)

[TINF20 AI1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8062001.ical?job=update_calendar)

[TINF20 AI2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8063001.ical?job=update_calendar)

[TINF20 CS1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8064001.ical?job=update_calendar)

[TINF20 IT1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8065001.ical?job=update_calendar)

[TINF20 IT2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8066001.ical?job=update_calendar)

[TINF21 AI1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8319001.ical?job=update_calendar)

[TINF21 AI2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8320001.ical?job=update_calendar)

[TINF21 CS1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8321001.ical?job=update_calendar)

[TINF21 CS2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8322001.ical?job=update_calendar)

[TINF21 IT1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8323001.ical?job=update_calendar)

## Courses WIB, WSTL, WIMBIT
[WIB18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7401001.ical?job=update_calendar)

[WIB18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7402001.ical?job=update_calendar)

[WIB18 BI](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7403001.ical?job=update_calendar)

[WSTL18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7416001.ical?job=update_calendar)

[WSTL18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7417001.ical?job=update_calendar)

[WSTL18 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7418001.ical?job=update_calendar)

[WSTL18 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7419001.ical?job=update_calendar)

[WIMBIT18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7456001.ical?job=update_calendar)

[WIMBIT18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7457001.ical?job=update_calendar)

[WIMBIT19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7738001.ical?job=update_calendar)

[WIMBIT19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7739001.ical?job=update_calendar)

[WIB19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7794001.ical?job=update_calendar)

[WIB19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7795001.ical?job=update_calendar)

[WSTL19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7801001.ical?job=update_calendar)

[WSTL19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7802001.ical?job=update_calendar)

[WSTL19 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7803001.ical?job=update_calendar)

[WSTL19 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7804001.ical?job=update_calendar)

[WIB19 BI](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7841001.ical?job=update_calendar)

[WIMBIT20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8032001.ical?job=update_calendar)

[WIMBIT20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8033001.ical?job=update_calendar)

[WIB20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8101001.ical?job=update_calendar)

[WIB20 Bi](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8102001.ical?job=update_calendar)

[WSTL20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8110001.ical?job=update_calendar)

[WSTL20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8111001.ical?job=update_calendar)

[WSTL20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8112001.ical?job=update_calendar)

[WSTL20 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8113001.ical?job=update_calendar)

[WIB21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8359001.ical?job=update_calendar)

[WIB21 BI](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8360001.ical?job=update_calendar)

[WSTL21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8368001.ical?job=update_calendar)

[WSTL21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8369001.ical?job=update_calendar)

[WSTL21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8370001.ical?job=update_calendar)

[WSTL21 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8371001.ical?job=update_calendar)

[WIMBIT21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8385001.ical?job=update_calendar)

[WIMBIT21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8386001.ical?job=update_calendar)


## Courses WOW, WWF, WGW, WST, WIW, WRSW
[WGW18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7395001.ical?job=update_calendar)

[WIW18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7410001.ical?job=update_calendar)

[WIW18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7411001.ical?job=update_calendar)

[WOW18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7414001.ical?job=update_calendar)

[WOW18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7415001.ical?job=update_calendar)

[WRSW18 AC1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7452001.ical?job=update_calendar)

[WRSW18 AC2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7453001.ical?job=update_calendar)

[WRSW18 ST1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7454001.ical?job=update_calendar)

[WRSW18 ST2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7455001.ical?job=update_calendar)

[WGW19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7782001.ical?job=update_calendar)

[WIW19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7787001.ical?job=update_calendar)

[WIW19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7788001.ical?job=update_calendar)

[WOW19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7799001.ical?job=update_calendar)

[WOW19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7800001.ical?job=update_calendar)

[WRSW19 AC1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7821001.ical?job=update_calendar)

[WRSW19 AC2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7822001.ical?job=update_calendar)

[WRSW19 ST1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7823001.ical?job=update_calendar)

[WRSW19 ST2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7824001.ical?job=update_calendar)

[WGW20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8090001.ical?job=update_calendar)

[WIW20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8094001.ical?job=update_calendar)

[WIW20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8095001.ical?job=update_calendar)

[WOW20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8108001.ical?job=update_calendar)

[WOW20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8109001.ical?job=update_calendar)

[WRSW20 AC1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8129001.ical?job=update_calendar)

[WRSW20 AC2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8130001.ical?job=update_calendar)

[WRSW20 ST1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8131001.ical?job=update_calendar)

[WRSW20 ST2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8132001.ical?job=update_calendar)

[GWAG20 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8206001.ical?job=update_calendar)

[WGW21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8348001.ical?job=update_calendar)

[WIW21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8352001.ical?job=update_calendar)

[WIW21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8353001.ical?job=update_calendar)

[WOW21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8366001.ical?job=update_calendar)

[WOW21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8367001.ical?job=update_calendar)

[WRSW21 AC1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8389001.ical?job=update_calendar)

[WRSW21 AC2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8390001.ical?job=update_calendar)

[WRSW21 ST1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8391001.ical?job=update_calendar)

[WRSW21 ST2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8392001.ical?job=update_calendar)


## Courses TWIW
[TWIW17 TV Pur](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7183001.ical?job=update_calendar)

[TWIW17 PL](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7184001.ical?job=update_calendar)

[TWIW17 TV/CV](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7185001.ical?job=update_calendar)

[TWIW17 EL](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7186001.ical?job=update_calendar)

[TWIW18 PL Pur](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7468001.ical?job=update_calendar)

[TWIW18 TV](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7469001.ical?job=update_calendar)

[TWIW18 CV/PL](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7470001.ical?job=update_calendar)

[TWIW18 EL](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7471001.ical?job=update_calendar)

[TWIW19 PL](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7774001.ical?job=update_calendar)

[TWIW19 TV](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7775001.ical?job=update_calendar)

[TWIW19 TV/CV](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7776001.ical?job=update_calendar)

[TWIW19 EL](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7777001.ical?job=update_calendar)

[TWIW20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8079001.ical?job=update_calendar)

[TWIW20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8080001.ical?job=update_calendar)

[TWIW20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8081001.ical?job=update_calendar)

[TWIW20 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8082001.ical?job=update_calendar)

[TWIW21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8336001.ical?job=update_calendar)

[TWIW21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8337001.ical?job=update_calendar)

[TWIW21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8338001.ical?job=update_calendar)

[TWIW21 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8339001.ical?job=update_calendar)


## Courses WVS
[WVS18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7420001.ical?job=update_calendar)

[WVS18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7421001.ical?job=update_calendar)

[WVS18 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7422001.ical?job=update_calendar)

[WVS18 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7423001.ical?job=update_calendar)

[WVS19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7805001.ical?job=update_calendar)

[WVS19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7806001.ical?job=update_calendar)

[WVS19 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7807001.ical?job=update_calendar)

[WVS20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8114001.ical?job=update_calendar)

[WVS20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8115001.ical?job=update_calendar)

[WVS20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8116001.ical?job=update_calendar)

[WVS21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8372001.ical?job=update_calendar)

[WVS21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8373001.ical?job=update_calendar)

[WVS21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8374001.ical?job=update_calendar)


## Courses WHD, WDLM, WMM
[WMM18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7390001.ical?job=update_calendar)

[WMM18B B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7391001.ical?job=update_calendar)

[WMM18 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7392001.ical?job=update_calendar)

[WHD18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7396001.ical?job=update_calendar)

[WHD18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7397001.ical?job=update_calendar)

[WHD18 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7398001.ical?job=update_calendar)

[WHD18 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7400001.ical?job=update_calendar)

[WHD19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7783001.ical?job=update_calendar)

[WHD19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7784001.ical?job=update_calendar)

[WHD19 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7785001.ical?job=update_calendar)

[WHD19 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7786001.ical?job=update_calendar)

[WMM19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7796001.ical?job=update_calendar)

[WMM19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7797001.ical?job=update_calendar)

[WMM19 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7798001.ical?job=update_calendar)

[WHD20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8091001.ical?job=update_calendar)

[WHD20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8092001.ical?job=update_calendar)

[WHD20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8093001.ical?job=update_calendar)

[WMM20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8103001.ical?job=update_calendar)

[WMM20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8104001.ical?job=update_calendar)

[WMM20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8105001.ical?job=update_calendar)

[WHD21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8349001.ical?job=update_calendar)

[WHD21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8350001.ical?job=update_calendar)

[WHD21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8351001.ical?job=update_calendar)

[WMM21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8361001.ical?job=update_calendar)

[WMM21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8362001.ical?job=update_calendar)

[WMM21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8363001.ical?job=update_calendar)


## Courses WIN
[WIN18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7404001.ical?job=update_calendar)

[WIN18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7405001.ical?job=update_calendar)

[WIN18 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7406001.ical?job=update_calendar)

[WIN18 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7407001.ical?job=update_calendar)

[WIN18 E](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7408001.ical?job=update_calendar)

[WIN18 F](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7409001.ical?job=update_calendar)

[WIN19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7789001.ical?job=update_calendar)

[WIN19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7790001.ical?job=update_calendar)

[WIN19 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7791001.ical?job=update_calendar)

[WIN19 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7792001.ical?job=update_calendar)

[WIN19 E](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7793001.ical?job=update_calendar)

[WIN20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8096001.ical?job=update_calendar)

[WIN20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8097001.ical?job=update_calendar)

[WIN20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8098001.ical?job=update_calendar)

[WIN20 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8099001.ical?job=update_calendar)

[WIN20 E](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8100001.ical?job=update_calendar)

[WIN21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8354001.ical?job=update_calendar)

[WIN21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8355001.ical?job=update_calendar)

[WIN21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8356001.ical?job=update_calendar)

[WIN21 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8357001.ical?job=update_calendar)

[WIN21 E](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8358001.ical?job=update_calendar)


## Courses WMKE, WMMK, WMPG
[WMKE18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7412001.ical?job=update_calendar)

[WMKE18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7413001.ical?job=update_calendar)

[WMMK18 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7450001.ical?job=update_calendar)

[WMPG18 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7451001.ical?job=update_calendar)

[WMKE19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7722001.ical?job=update_calendar)

[WMKE19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7723001.ical?job=update_calendar)

[WME19 MPG](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7819001.ical?job=update_calendar)

[WME19 MMK](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7820001.ical?job=update_calendar)

[WMKE20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8106001.ical?job=update_calendar)

[WMKE20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8107001.ical?job=update_calendar)

[WME20 MPG](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8127001.ical?job=update_calendar)

[WME20 MMK](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8128001.ical?job=update_calendar)

[WMKE21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8364001.ical?job=update_calendar)

[WMKE21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8365001.ical?job=update_calendar)

[WME21 MPG](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8387001.ical?job=update_calendar)

[WME21 MMK](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8388001.ical?job=update_calendar)


## Courses GWAG
[GWAG18 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7449001.ical?job=update_calendar)

[GWAG19 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7778001.ical?job=update_calendar)

[WAG20 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8083001.ical?job=update_calendar)

[GWAG20 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8206001.ical?job=update_calendar)

[GWAG21 .](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8340001.ical?job=update_calendar)


## Courses TMT,TEN
[TMT18 AM1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7443001.ical?job=update_calendar)

[TMT18 AM2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7444001.ical?job=update_calendar)

[TMT18 EN1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7445001.ical?job=update_calendar)

[TMT18 EN2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7446001.ical?job=update_calendar)

[TMT18 EW1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7447001.ical?job=update_calendar)

[TMT18 SI1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7448001.ical?job=update_calendar)

[TIE19 PE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7762001.ical?job=update_calendar)

[TIE19 SE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7763001.ical?job=update_calendar)

[TMT19 AM1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7771001.ical?job=update_calendar)

[TMT19 AM2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7772001.ical?job=update_calendar)

[TMT19 EW1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7773001.ical?job=update_calendar)

[TIE19 EN1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7906001.ical?job=update_calendar)

[TIE19 EN2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7907001.ical?job=update_calendar)

[TIE20 EN](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8067001.ical?job=update_calendar)

[TIE20 SE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8068001.ical?job=update_calendar)

[TMT20 AM1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8076001.ical?job=update_calendar)

[TMT20 AM2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8077001.ical?job=update_calendar)

[TMT20 EW1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8078001.ical?job=update_calendar)

[TMT21 AM1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8333001.ical?job=update_calendar)

[TMT21 AM2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8334001.ical?job=update_calendar)

[TMT21 EW1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8335001.ical?job=update_calendar)


## Courses TMB, TCT
[TCT 18](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7424001.ical?job=update_calendar)

[TMB18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7434001.ical?job=update_calendar)

[TMB18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7435001.ical?job=update_calendar)

[TMB18 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7436001.ical?job=update_calendar)

[TMB18 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7437001.ical?job=update_calendar)

[TMB18 F](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7439001.ical?job=update_calendar)

[TMB18 G](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7440001.ical?job=update_calendar)

[TMB18 HVT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7441001.ical?job=update_calendar)

[TMB18 KVEM](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7442001.ical?job=update_calendar)

[TCT 19](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7751001.ical?job=update_calendar)

[TMB19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7764001.ical?job=update_calendar)

[TMB19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7765001.ical?job=update_calendar)

[TMB19 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7766001.ical?job=update_calendar)

[TMB19 F](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7767001.ical?job=update_calendar)

[TMB19 G](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7768001.ical?job=update_calendar)

[TMB19 HVT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7769001.ical?job=update_calendar)

[TMB19 KVEM](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7770001.ical?job=update_calendar)

[TMB18 BV](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8039001.ical?job=update_calendar)

[TMB18 AM1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8040001.ical?job=update_calendar)

[TMB18 AM2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8041001.ical?job=update_calendar)

[TMB18 KT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8042001.ical?job=update_calendar)

[TMB18 PM](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8043001.ical?job=update_calendar)

[TMB18 PT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8044001.ical?job=update_calendar)

[TCT 20](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8056001.ical?job=update_calendar)

[TMB20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8069001.ical?job=update_calendar)

[TMB20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8070001.ical?job=update_calendar)

[TMB20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8071001.ical?job=update_calendar)

[TMB20 F](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8072001.ical?job=update_calendar)

[TMB20 G](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8073001.ical?job=update_calendar)

[TMB20 HVT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8074001.ical?job=update_calendar)

[TMB20 KVEM](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8075001.ical?job=update_calendar)

[TCT 21](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8312001.ical?job=update_calendar)

[TMB21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8326001.ical?job=update_calendar)

[TMB21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8327001.ical?job=update_calendar)

[TMB21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8328001.ical?job=update_calendar)

[TMB21 F](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8329001.ical?job=update_calendar)

[TMB21 G](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8330001.ical?job=update_calendar)

[TMB21 HVT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8331001.ical?job=update_calendar)

[TMB21 KVEM](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8332001.ical?job=update_calendar)

[TMB19 AM1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8394001.ical?job=update_calendar)

[TMB19 AM2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8395001.ical?job=update_calendar)

[TMB19 BV](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8396001.ical?job=update_calendar)

[TMB19 PM](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8397001.ical?job=update_calendar)

[TMB19 PT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8398001.ical?job=update_calendar)

[TMB19 KT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8399001.ical?job=update_calendar)


## Courses TEL
[TEL18 AAT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7425001.ical?job=update_calendar)

[TEL18 BAT](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7426001.ical?job=update_calendar)

[TEL18 AET](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7427001.ical?job=update_calendar)

[TEL18 AEU](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7428001.ical?job=update_calendar)

[TEL18 AMD](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7429001.ical?job=update_calendar)

[TEL20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8057001.ical?job=update_calendar)

[TEL20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8058001.ical?job=update_calendar)

[TEL20 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8059001.ical?job=update_calendar)

[TEL20 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8060001.ical?job=update_calendar)

[TEL20 E](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8061001.ical?job=update_calendar)

[TEL19 AT1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8210001.ical?job=update_calendar)

[TEL19 AT2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8211001.ical?job=update_calendar)

[TEL19 AT3](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8212001.ical?job=update_calendar)

[TEL19 EE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8213001.ical?job=update_calendar)

[TEL19 EO](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8214001.ical?job=update_calendar)

[TEL19 MED](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8215001.ical?job=update_calendar)

[TEL19 EU](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8216001.ical?job=update_calendar)

[TEL21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8313001.ical?job=update_calendar)

[TEL21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8314001.ical?job=update_calendar)

[TEL21 C](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8315001.ical?job=update_calendar)

[TEL21 D](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8316001.ical?job=update_calendar)

[TEL21 E](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8317001.ical?job=update_calendar)

[TEL21 F](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8318001.ical?job=update_calendar)

[TEL20 AT1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8403001.ical?job=update_calendar)

[TEL20 AT2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8404001.ical?job=update_calendar)

[TEL20 AT3](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8405001.ical?job=update_calendar)

[TEL20 EE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8406001.ical?job=update_calendar)

[TEL20 EU](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8407001.ical?job=update_calendar)

[TEL20 MED](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8408001.ical?job=update_calendar)


## Courses WBK, WFDL
[WFD17 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7179001.ical?job=update_calendar)

[WFD17 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7180001.ical?job=update_calendar)

[WBK18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7388001.ical?job=update_calendar)

[WBK18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7389001.ical?job=update_calendar)

[WFD18 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7393001.ical?job=update_calendar)

[WFD18 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7394001.ical?job=update_calendar)

[WBK19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7779001.ical?job=update_calendar)

[WBK19 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7780001.ical?job=update_calendar)

[WFD19 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7781001.ical?job=update_calendar)

[WBK20 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8084001.ical?job=update_calendar)

[WBK20 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8085001.ical?job=update_calendar)

[WBK21 A](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8341001.ical?job=update_calendar)

[WBK21 B](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8342001.ical?job=update_calendar)


## Courses WWI
[Pre- WI2018](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7368001.ical?job=update_calendar)

[WWI18 AMA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7458001.ical?job=update_calendar)

[WWI18 AMB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7459001.ical?job=update_calendar)

[WWI18 AMC](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7460001.ical?job=update_calendar)

[WWI18 DSA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7461001.ical?job=update_calendar)

[WWI18 DSB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7462001.ical?job=update_calendar)

[WWI18 SCA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7463001.ical?job=update_calendar)

[WWI18 SCB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7464001.ical?job=update_calendar)

[WWI18 SEA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7465001.ical?job=update_calendar)

[WWI18 SEB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7466001.ical?job=update_calendar)

[WWI18 SEC](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7467001.ical?job=update_calendar)

[Pre- WI2019](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7660001.ical?job=update_calendar)

[WWI19 AMA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7808001.ical?job=update_calendar)

[WWI19 AMB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7809001.ical?job=update_calendar)

[WWI19 AMC](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7810001.ical?job=update_calendar)

[WWI19 DSA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7811001.ical?job=update_calendar)

[WWI19 DSB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7812001.ical?job=update_calendar)

[WWI19 DSC](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7813001.ical?job=update_calendar)

[WWI19 EG/EH](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7814001.ical?job=update_calendar)

[WWI19 SCA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7815001.ical?job=update_calendar)

[WWI19 SCB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7816001.ical?job=update_calendar)

[WWI19 SEA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7817001.ical?job=update_calendar)

[WWI19 SEB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7818001.ical?job=update_calendar)

[WWI20 AMA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8117001.ical?job=update_calendar)

[WWI20 AMB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8118001.ical?job=update_calendar)

[WWI20 AMC](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8119001.ical?job=update_calendar)

[WWI20 DSA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8120001.ical?job=update_calendar)

[WWI20 DSB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8121001.ical?job=update_calendar)

[WWI20 EG/EH](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8122001.ical?job=update_calendar)

[WWI20 SCA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8123001.ical?job=update_calendar)

[WWI20 SCB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8124001.ical?job=update_calendar)

[WWI20 SEA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8125001.ical?job=update_calendar)

[WWI20 SEB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8126001.ical?job=update_calendar)

[WWI21 AMA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8375001.ical?job=update_calendar)

[WWI21 AMB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8376001.ical?job=update_calendar)

[WWI21 AMC](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8377001.ical?job=update_calendar)

[WWI21 DSA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8378001.ical?job=update_calendar)

[WWI21 DSB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8379001.ical?job=update_calendar)

[WWI21 EG/EH](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8380001.ical?job=update_calendar)

[WWI21 SCA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8381001.ical?job=update_calendar)

[WWI21 SCB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8382001.ical?job=update_calendar)

[WWI21 SEA](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8383001.ical?job=update_calendar)

[WWI21 SEB](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8384001.ical?job=update_calendar)


## IP (International Program)
[IP (International Program)](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/6126001.ical?job=update_calendar)


## Courses CAS Master
[CAS MasterCourses](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/6053001.ical?job=update_calendar)


## Courses TIE
[TIE19 SE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7763001.ical?job=update_calendar)

[TIE19 EN1](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7906001.ical?job=update_calendar)

[TIE19 EN2](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/7907001.ical?job=update_calendar)

[TIE20 EN](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8067001.ical?job=update_calendar)

[TIE20 SE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8068001.ical?job=update_calendar)

[TIE21 EN](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8324001.ical?job=update_calendar)

[TIE21 SE](https://gitlab.com/p-fruck/dhbw-ical-fix/-/jobs/artifacts/master/raw/ical/8325001.ical?job=update_calendar)
